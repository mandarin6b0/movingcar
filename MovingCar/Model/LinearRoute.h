//
//  LinearRoute.h
//  MovingCar
//
//  Created by Tien Nguyen on 29/10/2018.
//  Copyright © 2018 Tien. All rights reserved.
//

#ifndef LinearRoute_h
#define LinearRoute_h

#import "Route.h"

@interface LinearRoute : Route<AnimatableRoute>

- (id)initWithSourcePosition:(CGPoint)sourcePosition andTargetPosition:(CGPoint)targetPosition;

@end

#endif /* LinearRoute_h */
