//
//  Car.m
//  MovingCar
//
//  Created by Tien Nguyen on 29/10/2018.
//  Copyright © 2018 Tien. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Car.h"
#import "AxisRotationRoute.h"
#import "LinearRoute.h"


@interface Car ()

@property (readwrite, nonatomic, assign) CGPoint position;
@property (readwrite, nonatomic, assign) double angle;
@property (readwrite, nonatomic, assign) double rotationRadius;

@end


@implementation Car

- (id)initWithPosition:(CGPoint)position angle:(double)angle andRotationRadius:(double)rotationRadius {
    if (self = [super init]) {
        self.position = position;
        self.angle = angle;
        self.rotationRadius = rotationRadius;
    }
    return self;
}

/// move with axis rotation
/// maybe CGPoint here will be more approriate
- (NSArray<Route*>*)moveToX:(double)x andY:(double)y {
    NSMutableArray<Route*>* routes = [[NSMutableArray alloc] init];

    double offsetX = x - self.position.x;
    double offsetY = y - self.position.y;

    double targetAngle = [self radiansForOffsetX:offsetX andY:offsetY];
    if (self.angle < targetAngle - DBL_EPSILON || self.angle > targetAngle + DBL_EPSILON) {
        // need axis rotation
        [routes addObject:[[AxisRotationRoute alloc] initWithRotationFromAngle:self.angle to:targetAngle]];
        self.angle = targetAngle;
    }

    CGPoint targetPosition = CGPointMake(x, y);
    [routes addObject:[[LinearRoute alloc] initWithSourcePosition:self.position andTargetPosition:targetPosition]];
    self.position = targetPosition;

    return routes;
}

/// move with curve rotation
/// TODO: not implemented
- (NSArray<Route*>*)moveCurvativeToX:(double)x andY:(double)y {
    NSMutableArray<Route*>* routes = [[NSMutableArray alloc] init];

    double offsetX = x - self.position.x;
    double offsetY = y - self.position.y;

    double targetAngle = [self radiansForOffsetX:offsetX andY:offsetY];
    if (self.angle > targetAngle - DBL_EPSILON && self.angle < targetAngle + DBL_EPSILON) {
        // we don't need axis rotation
        CGPoint targetPosition = CGPointMake(x, y);
        [routes addObject:[[LinearRoute alloc] initWithSourcePosition:self.position andTargetPosition:targetPosition]];
        self.position = targetPosition;
        return routes;
    }

    // build curve route

    if ([self isInsideRotationArcTargetOffsetX:offsetX y:offsetY]) {
        // assuming that we can move behind without hindrance,
        // TODO: moving back until we satisfy minimal arc condition
        // [routes addObject: [self getRouteToMakeCurvativeRoutePossibleForTargetOffsetX:x andY:y]];
        [self getRouteToMakeCurvativeRoutePossibleForTargetOffsetX:offsetX andY:offsetY];

        // TODO: then build arc
    }

    // TODO: find rotation to build a tangent line with point
    // TODO: move straight

    return @[];
}

/// TODO: not implemented 
- (Route*)getRouteToMakeCurvativeRoutePossibleForTargetOffsetX:(double)x andY:(double)y {
    // TODO: complex math that I failed to finish in adequate time

    // // line formula: y = tg * x + b
    // // same line formula but intersect the offsetX,offsetY point: y = tg(angle) * x + b
    //
    // // perpendicular formula to line above: y = tg(angle + pi/2) * x + b
    // double b = y - tan(self.angle + M_PI_2) * x;
    // double intersectionX = b / (tan(self.angle) - tan(self.angle + M_PI_2));
    // double intersectionY = intersectionX * tan(self.angle + M_PI_2) + b;
    //
    // double diffX = intersectionX - x;
    // double diffY = intersectionY - y;
    // double intersectionDistance = sqrt( diffX*diffX + diffY*diffY );
    //
    // double sinA = intersectionDistance / self.rotationRadius;
    // double distanceFromIntersection = self.cornerRadius * cos( asin(sinA) );
    //
    // vc drawTestsPoints:
    //
    // double direction = 1;
    // if (isPointingPointX:intersectionX y:intersectionY) {
    //
    // }
    //
    // // determine where is back
    // return nil;

    return nil;
}

- (BOOL)isInsideRotationArcTargetOffsetX:(double)x y:(double)y {
    return x*x + y*y < self.rotationRadius*self.rotationRadius*4;
}


// [-M_PI ... M_PI]
- (double)radiansForOffsetX:(double)x andY:(double)y {
    double angle = atan(y / x);

    if (x < 0) {
        // 2nd and 3rd quarters, atan everytime returns angle as if
        // it is in 1st and 4th quarters, so we manually add pi
        angle += M_PI;
    }
    return angle;
}

@end
