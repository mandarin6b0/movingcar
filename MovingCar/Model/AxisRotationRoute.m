//
//  AxisRotationRoute.m
//  MovingCar
//
//  Created by Tien Nguyen on 29/10/2018.
//  Copyright © 2018 Tien. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AxisRotationRoute.h"

@interface AxisRotationRoute ()

// clockwise
@property (readwrite, nonatomic, assign) double sourceAngle;
@property (readwrite, nonatomic, assign) double targetAngle;

@end


@implementation AxisRotationRoute

- (id)initWithRotationFromAngle:(double)angle to:(double)targetAngle {
    if (self = [super init]) {
        self.sourceAngle = angle;
        self.targetAngle = targetAngle;
    }
    return self;
}

- (CABasicAnimation*)getAnimation {
    CABasicAnimation* animation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
    animation.fromValue = [NSNumber numberWithDouble:self.sourceAngle];

    double targetAngle = self.targetAngle;

    // minimize rotation
    double diff = self.sourceAngle - targetAngle;
    if (diff > M_PI) {
        targetAngle += 2*M_PI;
    }
    if (diff < -M_PI) {
        targetAngle -= 2*M_PI;
    }

    animation.toValue = [NSNumber numberWithDouble:targetAngle];

    // we can make dynamic duration by doing some work here
    // like diff*k
    animation.duration = 1;

    animation.removedOnCompletion = false;
    animation.fillMode = kCAFillModeForwards;

    return animation;
}

@end
