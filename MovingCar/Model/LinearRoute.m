//
//  LinearRoute.m
//  MovingCar
//
//  Created by Tien Nguyen on 29/10/2018.
//  Copyright © 2018 Tien. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LinearRoute.h"

@interface LinearRoute ()

@property (readwrite, nonatomic, assign) CGPoint sourcePosition;
@property (readwrite, nonatomic, assign) CGPoint targetPosition;

@end


@implementation LinearRoute

- (id)initWithSourcePosition:(CGPoint)sourcePosition andTargetPosition:(CGPoint)targetPosition {
    if (self = [super init]) {
        self.sourcePosition = sourcePosition;
        self.targetPosition = targetPosition;
    }
    return self;
}

- (CABasicAnimation*)getAnimation {
    CABasicAnimation* animation = [CABasicAnimation animationWithKeyPath:@"position"];
    animation.toValue = [NSValue valueWithCGPoint:self.targetPosition];

    // we can make duration dynamic by calcing distance between points
    animation.duration = 2;

    animation.removedOnCompletion = false;
    animation.fillMode = kCAFillModeForwards;
    return animation;
}

@end
