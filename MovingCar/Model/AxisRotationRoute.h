//
//  AxisRotationRoute.h
//  MovingCar
//
//  Created by Tien Nguyen on 29/10/2018.
//  Copyright © 2018 Tien. All rights reserved.
//

#ifndef AxisRotationRoute_h
#define AxisRotationRoute_h

#import "Route.h"


@interface AxisRotationRoute : Route<AnimatableRoute>

- (id)initWithRotationFromAngle:(double)angle to:(double)targetAngle;

@end


#endif /* AxisRotationRoute_h */
