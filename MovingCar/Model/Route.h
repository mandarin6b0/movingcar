//
//  Route.h
//  MovingCar
//
//  Created by Tien Nguyen on 29/10/2018.
//  Copyright © 2018 Tien. All rights reserved.
//

#ifndef Route_h
#define Route_h

#import <UIKit/UIKit.h>


@interface Route : NSObject
@end

@protocol AnimatableRoute <NSObject>

- (CABasicAnimation*)getAnimation;

@end


#endif /* Route_h */
