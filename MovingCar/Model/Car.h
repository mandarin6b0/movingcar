//
//  Car.h
//  MovingCar
//
//  Created by Tien Nguyen on 29/10/2018.
//  Copyright © 2018 Tien. All rights reserved.
//

#ifndef Car_h
#define Car_h

#import "Route.h"
#import <Foundation/Foundation.h>


@interface Car : NSObject

@property (readonly, nonatomic) CGPoint position;
@property (readonly, nonatomic) double angle;


- (id)initWithPosition:(CGPoint)position angle:(double)angle andRotationRadius:(double)rotationRadius;
- (NSArray<Route*>*)moveToX:(double)x andY:(double)y;
- (NSArray<Route*>*)moveCurvativeToX:(double)x andY:(double)y;

@end


#endif /* Car_h */
