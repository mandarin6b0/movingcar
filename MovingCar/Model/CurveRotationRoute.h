//
//  CurveRotationRoute.h
//  MovingCar
//
//  Created by Tien Nguyen on 29/10/2018.
//  Copyright © 2018 Tien. All rights reserved.
//

#ifndef CurveRotationRoute_h
#define CurveRotationRoute_h

#import "Route.h"


@interface CurveRotationRoute : Route

@end


#endif /* CurveRotationRoute_h */
