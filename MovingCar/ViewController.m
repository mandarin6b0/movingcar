//
//  ViewController.m
//  MovingCar
//
//  Created by Tien Nguyen on 29/10/2018.
//  Copyright © 2018 Tien. All rights reserved.
//



/*

Я реализовал довольно straightforward вариант, в основном чтобы потестить математику при подсчете роутов.
Соответственно есть Car класс, у которого есть 2 метода роутинга. 1ый с возможностью поворота по оси. 2ой без.
Пока реализовал 1ый метод только, который при вызове отдает до 2ух роутов (опционально AxisRotationRoute + LinearRoute).

В более продакшн окружении я бы реализовал несколько иначе, но так как математика там довольно комплексная,
которую я пока еще не решил, опишу архитектуру протоколами, без реализации:


//// MovableVehicle.h
@protocol MovableVehicle<NSObject>

// возвращает позицию транспорта, которая соответствует реальной позиции в данный момент
// в CGPoint используются координаты в системе транспорта, тобишь это может быть lon/lat тот же,
// для симплификации использую CGPoint
- (CGPoint)getCurrentPosition;

// двигает транспорта к точке
- (void)moveToPosition:(CGPoint);

- (bool)isMoving;

@end

// ниже опишу примерную стркутуру конкретного класса Car, соотственно в случае
// необходимости добавить авто, которая реализует второй вариант задачи, или любой другой транспортный объект,
// структура не меняется. только реализация moveToPosition и getCurrentPosition

//// AxisRotatableCar.h
@interface AxisRotatableCar : NSObject<MovableVehicle>
@end


//// AxisRotatableCar.m
@implementation AxisRotatableCar ()

// тут любые свойства, которые относятся к этому авто, и которая будет влиять на построение Route
@property (readwrite, nonatomic, assign) double rotationRadius;

// тут храним стейт авто - позиция, угол, ускорение, скорость
//
// state обновляется каждый раз при мутации(обнулении/перезаписи) routes массива:
// когда весь routes обошли (авто остановился), когда routes перезаписали новым (авто изменило движение)
//
// начальным значением является состояние авто при ините
@property (readwrite, nonatomic, assign) CarState state;

// тут храним массив Route, который
// 1) принимает сетится при вызове moveToPosition:
// 2) обнуляется каждый раз, когда весь routes обошли.
//    это можно сделать в getCurrentPosition, на основе времени, которое прошло с routesStartTimestamp
//    (когда время превышает время, нужное для обхода всех routes - обнуляем массив)
@property (readwrite, nonatomic, strong) NSArray<Route*>* routes;
@property (readwrite, nonatomic, assign) double routesStartTimestamp;

// реализация getCurrentPosition выглядет примерно как проход по routes массиву (если есть),
// нахождению нужного Route, исходя из startTimestamp и duration. и у нужного Route
// вызываем getPositionAfterTime:
//
// если routes nil - позицию берем из self.state;

// в реализации moveToPosition находится комплексная математика, которая должна разбить
// весь путь на примитивы Route и заполнить массив routes

// isMoving - это хелпер routes != nil, нужна для оптимизации анимации

@end


//// Route.h
@protocol Route

@property (readonly, nonatomic, assign) double startTimestamp;
@property (readonly, nonatomic, assign) double duration;

// для каждой конкретной реализации Route собираем в init максимальное кол-во данных
// чтобы могли getPositionAfterTime делать за O(1) и при этом крайне бытро
// (в идеале со скоростью fps экрана из-за реализации анимации)
//
- (CGPoint)getPositionAfterTime:(double)elapsedTime;

@end


//// VehiclesViewAdapter.h
@interface VehiclesViewAdapter : NSObject
- (void)moveCarWithId:(NSString*)id toPosition:(CGPoint)position;
@end

//// VehiclesViewAdapter.m
@interface VehiclesViewAdapter ()
@property (readwrite, nonatomic, strong) NSDictionary<NSString*, NSObject<Vehicle>*>* vehiclesById;
@end

@implementation VehiclesViewAdapter ()
@end

// тут происходит анимация. анимация делается на основе CADisplayLink.
// на каждом тике происходит пробежка по массиву движущихся (isMoving) транспортов
// и обновление фреймов вьюх к транспорту
//
// если транспорты все стоят - invalidate делаем. если появился движущийся транспорт
// перезапускаем CADisplayLink

// вьюхи к транспорту реализуются по типу UITableView с register/deque

// тут же происходит трансформация координат транспорта во фрейм вьюхи транспорта (lon/lat в x/y)

// в moveCarWithId:toPosition: вызываем у Vehicle moveToPosition: и запускаем CADisplayLink, если еще не запущен



В целом получается такие депенденси
VC  ->  VehiclesViewAdapter         ->         Vehicle
  (tap)                      (moveToPosition)

CADisplayLink      ->         VehicleView
    |         (updateFrames)
    |
    | (getCurrentPosition)
    V
 Vehicle


в такой архитектуре модельная часть полностью отделена от UI - что значит
комплексную математику можно начать обкладывать тестами в самом начале реализации

при необходимости добавления параметров транспорта (макс скорость, макс ускорение),
нужно только расширить конкретный класс транспорта (по факту moveToPosition метод расширить).

есть также в архитектуре минус в виде moveToPosition, который должен быть реализован
у каждого конкретного класса. в теории если Vehicle умеет делать один и тот же набор Route,
то реализацию можно было бы вынести (swift extension вместе с constraint). но тут нужно
смотреть на конкретику задачи. слишком сильная универсализация может стать боком при необходимости
тюнинга конкретного класса.

между VC и VehiclesViewAdapter, в случае если с сервера прилетают Vehicle, я бы добавил слой ViewModel.
сейчас же я не вижу большого профита от него.

я буду на досуге обновлять этот проект при необходимости. как только решу математику за moveToPosition :)

**/




#import "ViewController.h"
#import "Car.h"
#import "Route.h"

@interface ViewController ()

@property (readwrite, nonatomic, strong) Car* car;
@property (readwrite, nonatomic, strong) UIView* carView;
@property (readwrite, nonatomic, assign) BOOL isAnimating;

@end

@implementation ViewController


- (void)loadView {
    [super loadView];

    self.carView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 40, 20)];
    self.carView.backgroundColor = UIColor.blackColor;
    [self.view addSubview:self.carView];

    CALayer* carHeadLayer = [[CALayer alloc] init];
    carHeadLayer.backgroundColor = [[UIColor greenColor] CGColor];
    carHeadLayer.frame = CGRectMake(35, 5, 10, 10);
    [self.carView.layer addSublayer:carHeadLayer];
}


- (void)viewDidLoad {
    [super viewDidLoad];

    CGPoint position = CGPointMake(CGRectGetMidX(self.view.bounds), CGRectGetMidY(self.view.bounds));

    self.car = [[Car alloc] initWithPosition:position angle:-M_PI_2 andRotationRadius: 50];

    self.carView.center = self.car.position;
    self.carView.layer.affineTransform = CGAffineTransformMakeRotation(self.car.angle);

    UITapGestureRecognizer* tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(_handleTap:)];
    [self.view addGestureRecognizer:tapGesture];
}


- (void)_handleTap:(UITapGestureRecognizer *)gesture {
    if (gesture.state != UIGestureRecognizerStateEnded || self.isAnimating) {
        return;
    }

    CGPoint location = [gesture locationInView:self.view];

    NSArray<Route*>* routes = [self.car moveToX:location.x andY:location.y];

    self.isAnimating = true;
    NSMutableArray<CAAnimation*>* animations = [[NSMutableArray alloc] init];
    for (int i=0; i < routes.count; i++) {
        CAAnimation* animation = [((Route<AnimatableRoute>*)routes[i]) getAnimation];
        if (animation != nil) {
            [animations addObject:animation];
        }
    }

    __weak typeof(self) weakSelf = self;
    [self tearDownAnimations:animations withCompletion:^{
        __strong typeof(self) strongSelf = weakSelf;
        strongSelf.carView.layer.affineTransform = CGAffineTransformMakeRotation(strongSelf.car.angle);
        strongSelf.carView.center = strongSelf.car.position;

        [strongSelf.carView.layer removeAllAnimations];
        strongSelf.isAnimating = false;
    }];
}


- (void)tearDownAnimations:(NSMutableArray<CAAnimation*>*)animations withCompletion:(void (^)(void))completion {
    if (animations.count == 0) {
        completion();
        return;
    }

    CAAnimation* animation = [animations objectAtIndex:0];

    // O(n) here. probably we should reverse the array at the calling side, then
    // we will achieve O(1)
    [animations removeObjectAtIndex:0];

    [CATransaction begin];

    __weak typeof(self) weakSelf = self;
    [CATransaction setCompletionBlock:^{
        [weakSelf tearDownAnimations:animations withCompletion:completion];
    }];

    [self.carView.layer addAnimation:animation forKey:[NSString stringWithFormat:@"animation-%lu", (unsigned long)animations.count]];

    [CATransaction commit];
}


@end
