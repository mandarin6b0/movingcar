//
//  AppDelegate.h
//  MovingCar
//
//  Created by Tien Nguyen on 29/10/2018.
//  Copyright © 2018 Tien. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

